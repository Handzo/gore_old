package gore

import (
	"encoding/json"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/status"
)

type GRPCHandler func(*http.Request) (interface{}, error)

func (h GRPCHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	resp, err := h(r)
	if err != nil {
		grpcErrorResponse(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		grpcErrorResponse(w, err)
		return
	}
}

func grpcErrorResponse(w http.ResponseWriter, err error) {
	var e Error
	st, ok := status.FromError(err)
	if !ok {
		e = InternalError("Unhandled error", err)
	} else {
		httpStatus := runtime.HTTPStatusFromCode(st.Code())
		e = NewError(httpStatus, st.Message(), st.Err())
	}
	w.WriteHeader(e.Code)
	json.NewEncoder(w).Encode(e)
}
