package gore

import "github.com/sirupsen/logrus"

type Logger interface {
	WithField(key string, value interface{}) Logger
	WithError(err error) Logger

	Debugf(format string, args ...interface{})
	Infof(format string, args ...interface{})
	Printf(format string, args ...interface{})
	Warnf(format string, args ...interface{})
	Errorf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
	Panicf(format string, args ...interface{})

	Debug(args ...interface{})
	Info(args ...interface{})
	Print(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
	Panic(args ...interface{})

	Debugln(args ...interface{})
	Infoln(args ...interface{})
	Println(args ...interface{})
	Warnln(args ...interface{})
	Errorln(args ...interface{})
	Fatalln(args ...interface{})
	Panicln(args ...interface{})
}

type logrusWrapper struct {
	log *logrus.Entry
}

func NewLogrusWrapper(entry *logrus.Entry) *logrusWrapper {
	return &logrusWrapper{entry}
}

func (l *logrusWrapper) WithField(key string, value interface{}) Logger {
	return &logrusWrapper{l.log.WithField(key, value)}
}

func (l *logrusWrapper) WithError(err error) Logger {
	return &logrusWrapper{l.log.WithError(err)}
}

func (l *logrusWrapper) Debugf(format string, args ...interface{}) {
	l.log.Debugf(format, args...)
}

func (l *logrusWrapper) Infof(format string, args ...interface{}) {
	l.log.Infof(format, args...)
}

func (l *logrusWrapper) Printf(format string, args ...interface{}) {
	l.log.Printf(format, args...)
}

func (l *logrusWrapper) Warnf(format string, args ...interface{}) {
	l.log.Warnf(format, args...)
}

func (l *logrusWrapper) Errorf(format string, args ...interface{}) {
	l.log.Errorf(format, args...)
}

func (l *logrusWrapper) Fatalf(format string, args ...interface{}) {
	l.log.Fatalf(format, args...)
}

func (l *logrusWrapper) Panicf(format string, args ...interface{}) {
	l.log.Panicf(format, args...)
}

func (l *logrusWrapper) Debug(args ...interface{}) {
	l.log.Debug(args...)
}

func (l *logrusWrapper) Info(args ...interface{}) {
	l.log.Info(args...)
}

func (l *logrusWrapper) Print(args ...interface{}) {
	l.log.Print(args...)
}

func (l *logrusWrapper) Warn(args ...interface{}) {
	l.log.Warn(args...)
}

func (l *logrusWrapper) Error(args ...interface{}) {
	l.log.Error(args...)
}

func (l *logrusWrapper) Fatal(args ...interface{}) {
	l.log.Fatal(args...)
}

func (l *logrusWrapper) Panic(args ...interface{}) {
	l.log.Panic(args...)
}

func (l *logrusWrapper) Debugln(args ...interface{}) {
	l.log.Debugln(args...)
}

func (l *logrusWrapper) Infoln(args ...interface{}) {
	l.log.Infoln(args...)
}

func (l *logrusWrapper) Println(args ...interface{}) {
	l.log.Println(args...)
}

func (l *logrusWrapper) Warnln(args ...interface{}) {
	l.log.Warnln(args...)
}

func (l *logrusWrapper) Errorln(args ...interface{}) {
	l.log.Errorln(args...)
}

func (l *logrusWrapper) Fatalln(args ...interface{}) {
	l.log.Fatalln(args...)
}

func (l *logrusWrapper) Panicln(args ...interface{}) {
	l.log.Panicln(args...)
}
