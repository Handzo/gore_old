package gore

import (
	"io/ioutil"
	"os"
)

func ReadAllFile(file string) ([]byte, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	return ioutil.ReadAll(f)
}
