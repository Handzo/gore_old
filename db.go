package gore

import (
	"context"
)

type Transaction interface {
	Commit() error
	CommitContext(context.Context) error
	Rollback() error
	RollbackContext(context.Context) error
}
