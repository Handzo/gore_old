package gore

import "net/http"

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Err     error  `json:"-"`
}

func (e Error) Error() string {
	return e.Error()
}

func NewError(code int, message string, err error) Error {
	return Error{
		Code:    code,
		Message: message,
		Err:     err,
	}
}

func InternalError(message string, err error) Error {
	return NewError(http.StatusInternalServerError, message, err)
}

func BadRequest(message string, err error) Error {
	return NewError(http.StatusBadRequest, message, err)
}
