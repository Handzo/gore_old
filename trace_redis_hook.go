package gore

import (
	"context"

	"github.com/go-redis/redis/extra/rediscmd"
	"github.com/go-redis/redis/v8"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
)

type TraceRedisHook struct{}

func (rh TraceRedisHook) BeforeProcess(ctx context.Context, cmd redis.Cmder) (context.Context, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span = span.Tracer().StartSpan(cmd.FullName(), opentracing.ChildOf(span.Context()))
		span.SetTag(string(ext.DBType), "redis").
			SetTag(string(ext.DBStatement), rediscmd.CmdString(cmd))
		ctx = opentracing.ContextWithSpan(ctx, span)
	}
	return ctx, nil
}

func (rh TraceRedisHook) AfterProcess(ctx context.Context, cmd redis.Cmder) error {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		defer span.Finish()

		if err := cmd.Err(); err != nil {
			span.LogFields(log.Error(err))
		}
	}
	return nil
}

func (rh TraceRedisHook) BeforeProcessPipeline(ctx context.Context, cmds []redis.Cmder) (context.Context, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		summary, cmdsString := rediscmd.CmdsString(cmds)

		span = span.Tracer().StartSpan("pipeline "+summary, opentracing.ChildOf(span.Context()))
		span.SetTag(string(ext.DBType), "redis").
			SetTag(string(ext.DBStatement), cmdsString).
			SetTag("db.num_cmd", len(cmds))
		ctx = opentracing.ContextWithSpan(ctx, span)
	}
	return ctx, nil
}

func (rh TraceRedisHook) AfterProcessPipeline(ctx context.Context, cmds []redis.Cmder) error {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		defer span.Finish()

		if err := cmds[0].Err(); err != nil {
			span.LogFields(log.Error(err))
		}
	}
	return nil
}
