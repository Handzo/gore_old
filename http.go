package gore

import (
	"encoding/json"
	"net/http"

	"github.com/opentracing/opentracing-go"
)

type Handler func(*http.Request) (interface{}, error)

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	resp, err := h(r)
	if err != nil {
		errorResponse(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		errorResponse(w, err)
		return
	}
}

func errorResponse(w http.ResponseWriter, err error) {
	e, ok := err.(Error)
	if !ok {
		e = InternalError("Unhandled error", err)
	}
	w.WriteHeader(e.Code)
	json.NewEncoder(w).Encode(e)
}

func CORS(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}

func InjectTraceHTTP(req *http.Request) *http.Request {
	ctx := req.Context()

	if span := opentracing.SpanFromContext(ctx); span != nil {
		// Transmit the span's TraceContext as HTTP headers
		span.Tracer().Inject(
			span.Context(),
			opentracing.HTTPHeaders,
			opentracing.HTTPHeadersCarrier(req.Header))

		ctx = opentracing.ContextWithSpan(ctx, span)
	}
	return req.WithContext(ctx)
}

func ExtractTraceHTTP(req *http.Request, tracer opentracing.Tracer) (opentracing.SpanContext, error) {
	return tracer.Extract(
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(req.Header))

}
