package gore

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
)

// postgres models

type model struct {
	CreatedAt time.Time  `pg:",notnull,default:now()"`
	UpdatedAt time.Time  `pg:",notnull,default:now()"`
	DeletedAt *time.Time `json:"-"`
}

type Model struct {
	Id int `pg:",pk"`
	model
}

func (m *Model) AfterUpdate(ctx context.Context) (context.Context, error) {
	m.UpdatedAt = time.Now()
	return ctx, nil
}

type ModelUUID struct {
	Id string `pg:",pk,type:uuid,default:uuid_generate_v4()" json:"id"`
	model
}

func (m *ModelUUID) AfterUpdate(ctx context.Context) (context.Context, error) {
	m.UpdatedAt = time.Now()
	return ctx, nil
}

// queries

func CreateEnum(db pg.DBI, force bool, name string, columns ...string) error {
	exists := false

	if _, err := db.Query(pg.Scan(&exists), "SELECT EXISTS (SELECT true FROM pg_type WHERE typname = ?);", name); err != nil {
		return err
	}

	if exists && force {
		if _, err := db.Exec(fmt.Sprintf(`DROP TYPE %s CASCADE;`, name)); err != nil {
			return err
		}
		exists = false
	}

	if exists {
		return nil
	}

	cols := make([]string, len(columns))
	for i, col := range columns {
		cols[i] = "'" + col + "'"
	}

	if _, err := db.Exec(fmt.Sprintf(`CREATE TYPE %s AS ENUM (%s)`, name, strings.Join(cols, ","))); err != nil {
		return err
	}

	return nil
}

func CreateConstraint(db pg.DBI, force bool, tableName, constraintName, constraint string) error {
	exists := false

	if _, err := db.Query(pg.Scan(&exists), `SELECT EXISTS (SELECT true FROM information_schema.constraint_column_usage WHERE table_name = ? and constraint_name = ?);`, tableName, constraintName); err != nil {
		return err
	}

	if exists && force {
		if _, err := db.Exec(fmt.Sprintf(`ALTER TABLE %s DROP CONSTRAINT %s;`, tableName, constraintName)); err != nil {
			return err
		}

		exists = false
	}

	if exists {
		return nil
	}

	if _, err := db.Exec(fmt.Sprintf(`ALTER TABLE %s ADD CONSTRAINT %s CHECK(%s)`, tableName, constraintName, constraint)); err != nil {
		return nil
	}

	return nil
}

// postgres tracing query hook

type TracePostgresHook struct{}

var _ pg.QueryHook = (*TracePostgresHook)(nil)

type queryOperation interface {
	Operation() orm.QueryOp
}

func (h TracePostgresHook) BeforeQuery(ctx context.Context, evt *pg.QueryEvent) (context.Context, error) {
	var span opentracing.Span
	if span = opentracing.SpanFromContext(ctx); span != nil {
		_, ctx = opentracing.StartSpanFromContextWithTracer(ctx, span.Tracer(), "query")
	}

	return ctx, nil
}

func (h TracePostgresHook) AfterQuery(ctx context.Context, evt *pg.QueryEvent) error {
	span := opentracing.SpanFromContext(ctx)
	if span == nil {
		return nil
	}
	defer span.Finish()

	var operation orm.QueryOp

	if v, ok := evt.Query.(queryOperation); ok {
		operation = v.Operation()
	}

	// var query string
	// if operation == orm.InsertOp {
	// 	b, err := evt.UnformattedQuery()
	// 	if err != nil {
	// 		return err
	// 	}
	// 	query = string(b)
	// } else {
	b, err := evt.FormattedQuery()
	if err != nil {
		return err
	}
	query := string(b)
	// }

	if operation != "" {
		span.SetOperationName(string(operation))
	} else {
		name := query
		if idx := strings.IndexByte(name, ' '); idx > 0 {
			name = name[:idx]
		}
		if len(name) > 20 {
			name = name[:20]
		}
		span.SetOperationName(name)
	}

	const queryLimit = 5000
	if len(query) > queryLimit {
		query = query[:queryLimit]
	}

	span.SetTag(string(ext.DBType), "postgres")
	span.LogFields(log.String(string(ext.DBStatement), query))

	if db, ok := evt.DB.(*pg.DB); ok {
		opt := db.Options()

		span.SetTag(string(ext.DBUser), opt.User).
			SetTag(string(ext.DBInstance), opt.Database)
	}

	if evt.Err != nil {
		switch evt.Err {
		case pg.ErrNoRows, pg.ErrMultiRows:
			// nothing
		default:
			span.LogFields(log.Error(evt.Err))
		}
	} else if evt.Result != nil {
		numRow := evt.Result.RowsAffected()
		if numRow == 0 {
			numRow = evt.Result.RowsReturned()
		}
		span.SetTag("db.rows_affected", numRow)
	}

	return nil
}

type PGModel interface {
	BeforeSync(pg.DBI, bool) error
	Sync(pg.DBI, bool) error
	AfterSync(pg.DBI, bool) error
}

func SyncPostgres(db pg.DBI, tables []PGModel, force bool) error {
	for _, t := range tables {
		if force {
			db.Model(t).DropTable(&orm.DropTableOptions{Cascade: true, IfExists: true})
		}
		if err := t.BeforeSync(db, force); err != nil {
			return err
		}
		if err := db.Model(t).CreateTable(&orm.CreateTableOptions{IfNotExists: true, FKConstraints: true}); err != nil {
			return err
		}
		if err := t.Sync(db, force); err != nil {
			return err
		}
		if err := t.AfterSync(db, force); err != nil {
			return err
		}
	}

	return nil
}
