package gore

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	"github.com/uber/jaeger-client-go/config"
)

func NewTracerFromEnv() (opentracing.Tracer, io.Closer, error) {
	cfg, err := config.FromEnv()
	if err != nil {
		return nil, nil, err
	}

	return cfg.NewTracer()
}

type traceResponseWriter struct {
	http.ResponseWriter
	span opentracing.Span
}

func (t traceResponseWriter) Write(body []byte) (int, error) {
	t.span.LogFields(log.String("response", string(body)))
	return t.ResponseWriter.Write(body)
}

func (t traceResponseWriter) WriteHeader(statusCode int) {
	t.span.SetTag(string(ext.HTTPStatusCode), statusCode)
	if statusCode != http.StatusOK {
		t.span.SetTag(string(ext.Error), true)
	}
	t.ResponseWriter.WriteHeader(statusCode)
}

func TraceMiddleware(tracer opentracing.Tracer) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			spanCtx, _ := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
			span := tracer.StartSpan(r.URL.String(), opentracing.ChildOf(spanCtx))
			defer span.Finish()

			span.SetTag(string(ext.HTTPMethod), r.Method)
			span.SetTag(string(ext.HTTPUrl), r.URL.String())

			tracer.Inject(
				span.Context(),
				opentracing.HTTPHeaders,
				opentracing.HTTPHeadersCarrier(r.Header))

			body, err := ioutil.ReadAll(r.Body)
			if err != nil && err != io.EOF {
				span.SetTag(string(ext.Error), true)
			}

			span.LogFields(log.String("body", string(body)))

			r.Body = ioutil.NopCloser(bytes.NewReader(body))

			ctx := opentracing.ContextWithSpan(r.Context(), span)

			tw := &traceResponseWriter{w, span}

			next.ServeHTTP(tw, r.WithContext(ctx))
		})
	}
}
