package gore

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
)

func NewRedis(options *redis.Options) *redis.Client {
	rdb := redis.NewClient(options)

	if _, err := rdb.Ping(context.Background()).Result(); err != nil {
		panic(err)
	}

	return rdb
}

type redisCache struct {
	client *redis.Client
}

func NewRedisCache(client *redis.Client) *redisCache {
	return &redisCache{client}
}

func (r *redisCache) Set(ctx context.Context, key, value string, expiration time.Duration) error {
	return r.client.Set(ctx, key, value, expiration).Err()
}

func (r *redisCache) Get(ctx context.Context, key string) (string, error) {
	return r.client.Get(ctx, key).Result()
}

func (r *redisCache) Del(ctx context.Context, keys ...string) (int64, error) {
	return r.client.Del(ctx, keys...).Result()
}
