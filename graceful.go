package gore

import (
	"io"
	"os"
	"os/signal"

	"google.golang.org/grpc"
)

func Graceful(logger Logger, signals []os.Signal, closeItems ...io.Closer) {
	sigch := make(chan os.Signal)
	signal.Notify(sigch, signals...)

	sig := <-sigch

	logger.Warnf("Caught signal %s, gracefully shutting down...", sig)

	for i := len(closeItems) - 1; i >= 0; i-- {
		closer := closeItems[i]
		if err := closer.Close(); err != nil {
			logger.Errorf("Faild to close: %v: %v\n", closer, err)
		}
	}
}

type gRPCCloser struct {
	server *grpc.Server
}

func (g *gRPCCloser) Close() error {
	g.server.GracefulStop()
	return nil
}

func NewGRPCCloser(server *grpc.Server) *gRPCCloser {
	return &gRPCCloser{server}
}
